#ifndef __WEB_SERVER_PLUGIN__
#define __WEB_SERVER_PLUGIN__

#include <FS.h>
#include <ESPAsyncWebServer.h>
#include "TaskSchedulerDeclarations.h"
#include "Plugin.h"
#include "WebServerConfig.h"
#include "utils/print.h"

using namespace std;
using namespace std::placeholders;

class WebServerPlugin : public Plugin
{
  private:
    WebServerConfig config;

  public:
    AsyncWebServer *server;
    WebServerPlugin(WebServerConfig cfg);
    WebServerPlugin(WebServerConfig cfg, AsyncWebServer *webServer);
    void activate(Scheduler *userScheduler);
};

#endif