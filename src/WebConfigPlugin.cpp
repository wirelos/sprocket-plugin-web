#include "WebConfigPlugin.h"

WebConfigPlugin::WebConfigPlugin(AsyncWebServer *webServer)
{
    server = webServer;
    server->serveStatic("/config/wifi.json", SPIFFS, "config/wifi.json");
}
void WebConfigPlugin::activate(Scheduler *userScheduler)
{
    server->on("/heap", HTTP_GET, [](AsyncWebServerRequest *request) {
        PRINT_MSG(Serial, "WEB", "GET /heap");
        request->send(200, "text/plain", String(ESP.getFreeHeap()));
    });
    server->on("/restart", HTTP_POST, [](AsyncWebServerRequest *request) {
        PRINT_MSG(Serial, "WEB", "POST /restart");
        ESP.restart();
    });
    server->on("/config", HTTP_POST, [](AsyncWebServerRequest *request) {
        PRINT_MSG(Serial, "WEB", "POST /config");
        if (request->hasParam("config", true) && request->hasParam("fileName", true))
        {
            String inStr = request->getParam("config", true)->value();
            String fileName = request->getParam("fileName", true)->value();
            File f = SPIFFS.open(fileName, "w");
            if (!f)
            {
                PRINT_MSG(Serial, "WEB", String(String("file open for read failed: ") + fileName).c_str());
            }
            PRINT_MSG(Serial, "WEB", String(String("writing to SPIFFS file: ") + fileName).c_str());
            f.print(inStr);
            f.close();
        }
        request->redirect("/");
    });
    PRINT_MSG(Serial, "WEB", "WebConfig activated");
}