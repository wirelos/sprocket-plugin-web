#ifndef __WEB_CONFIG_PLUGIN_H__
#define __WEB_CONFIG_PLUGIN_H__

#include <FS.h>
#include <ESPAsyncWebServer.h>
#include <TaskSchedulerDeclarations.h>
#include "Plugin.h"
#include "WebServerConfig.h"
#include "utils/print.h"

using namespace std;
using namespace std::placeholders;

class WebConfigPlugin : public Plugin
{
  private:
    AsyncWebServer *server;

  public:
    WebConfigPlugin(AsyncWebServer *webServer);
    void activate(Scheduler *userScheduler);
};

#endif