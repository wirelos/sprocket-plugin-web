#ifndef __WEBAPI_PLUGIN__
#define __WEBAPI_PLUGIN__

#include <TaskSchedulerDeclarations.h>
#include <Sprocket.h>
#ifdef ESP32
#include <Update.h>
#elif defined(ESP8266)
#include <Updater.h>
#endif
#include "config.h"
#include "utils/print.h"
#include "WebUtils.h"
#include "WebServerPlugin.h"
#include "WebConfigPlugin.h"

using namespace std;
using namespace std::placeholders;

class WebApiPlugin : public Plugin
{

  public:
    AsyncWebServer *server;
    AsyncWebSocket *ws;
    SprocketMessage currentMessage;

    WebApiPlugin(AsyncWebServer *_server);
    void activate(Scheduler *_scheduler);
    void wsBroadcast(String msg);
    void postRequestHandler(AsyncWebServerRequest *request);
    void onWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len);
    void simpleFirmwareUploadForm(AsyncWebServerRequest *request);
    void onFirmwareUpdateRequest(AsyncWebServerRequest *request);
    void onFirmwareUpload(AsyncWebServerRequest *request, const String &filename, size_t index, uint8_t *data, size_t len, bool final);
    void dispatch(String &msg);
};

#endif