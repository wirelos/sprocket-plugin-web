#ifndef __WEB_SERVER_CONFIG__
#define __WEB_SERVER_CONFIG__

struct WebServerConfig
{
    const char *contextPath;
    const char *docRoot;
    const char *defaultFile;
    int port;
    int useBasicAuth;
    const char *user;
    const char *password;
};

#endif