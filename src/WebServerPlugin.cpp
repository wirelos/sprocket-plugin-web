#include "WebServerPlugin.h"

WebServerPlugin::WebServerPlugin(WebServerConfig cfg)
{
    config = cfg;
    server = new AsyncWebServer(config.port);
}
WebServerPlugin::WebServerPlugin(WebServerConfig cfg, AsyncWebServer *webServer)
{
    config = cfg;
    server = webServer;
}
void WebServerPlugin::activate(Scheduler *userScheduler)
{
    AsyncStaticWebHandler &staticWebHandler = server->serveStatic(config.contextPath, SPIFFS, config.docRoot).setDefaultFile(config.defaultFile);
    if (config.useBasicAuth)
    {
        staticWebHandler.setAuthentication(config.user, config.password);
    }
    server->begin();
    PRINT_MSG(Serial, "WEB", "Server activated");
}
