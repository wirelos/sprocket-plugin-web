#include "config.h"
#include "WiFiNet.h"
#include "Sprocket.h"
#include "ESPAsyncWebServer.h"

#include "WebServerConfig.h"
#include "WebServerPlugin.h"
#include "WebConfigPlugin.h"
#include "WebApiPlugin.h"

WiFiNet *network;
Sprocket *sprocket;
AsyncWebServer *server;
WebServerPlugin *webServerPlugin;
WebConfigPlugin *webConfigPlugin;
WebApiPlugin *webApiPlugin;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    webServerPlugin = new WebServerPlugin({"/", "/www", "index.html", 80});
    webConfigPlugin = new WebConfigPlugin(webServerPlugin->server);
    webApiPlugin = new WebApiPlugin(webServerPlugin->server);

    sprocket->addPlugin(webServerPlugin);
    sprocket->addPlugin(webConfigPlugin);
    sprocket->addPlugin(webApiPlugin);

    network = new WiFiNet(
        SPROCKET_MODE,
        STATION_SSID,
        STATION_PASSWORD,
        AP_SSID,
        AP_PASSWORD,
        HOSTNAME,
        CONNECT_TIMEOUT);
    network->connect();

    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}