#ifndef __WebUtils_H___
#define __WebUtils_H___

#include <Arduino.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
//#ifdef ESP32
//#include <AsyncTCP.h>
//#elif defined(ESP8266)
#include <ESPAsyncTCP.h>
//#endif

class WebUtils
{
  public:
    static String getRequestParameterOrDefault(AsyncWebServerRequest *request, String param, String defaultValue, bool isPost = true);
    static String parseFrame(AwsEventType type, void *arg, uint8_t *data, size_t len);
    static String parseFrameAsString(AwsEventType type, void *arg, uint8_t *data, size_t len, int start = 0);
    static void wsEventPrint(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len);
};

#endif